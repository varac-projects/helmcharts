# Varac's helm charts

![](https://i.pinimg.com/originals/5f/05/f5/5f05f5c5213b9725ca6336fe8f85085c.jpg)

## Usage

    $ helm repo add varac https://0xacab.org/api/v4/projects/2332/packages/helm/stable
    $ helm search repo varac
    $ helm install varac/<CHART>

## Charts

* [personal-gitlab-exporter](https://0xacab.org/varac/helmcharts/tree/master/personal-gitlab-exporter): Prometheus exporter for personal gilab metrics, see [https://0xacab.org/varac/personal-gitlab-exporter](https://0xacab.org/varac/personal-gitlab-exporter)
* [restic-rest-server](https://0xacab.org/varac/helmcharts/-/tree/master/restic-rest-server): Rest Server is a high performance HTTP server that implements restic's REST backend API (https://github.com/restic/rest-server)
* [tmate-ssh-server](https://0xacab.org/varac/helmcharts/tree/master/tmate-ssh-server): [tmate.io](https://tmate.io/) ssh-server

## Sources

See [https://0xacab.org/varac/helmcharts](https://0xacab.org/varac/helmcharts)
for the chart git repository.
